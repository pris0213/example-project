Feature: My Feature

@example_tag
Scenario Outline: A scenario
    Given this is a scenario
    When I save the file
    Then the tag above won't follow the color scheme for tags on feature files
    
@example_tag    
Scenario Outline: Another scenario
    Given this is the second scenario
    When I save the file
    Then the tag above will be blue